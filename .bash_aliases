# some more ls aliases
alias ll='ls -l'
alias la='ls -a'
alias l='ls -CF'
alias lh='ls -lh'

alias vgrep='grep -v'
alias igrep='grep -i'
alias ivgrep='grep -i -v'

alias grepnc='grep -n --col'

# Administrative aliases
alias aptitude_non_repo='aptitude search "~S~i(!~Aexperimental!~Aunstable!~Atesting!~Astable)"'

alias aptitude_debian_non_repo='aptitude_non_repo | grep -v -e heimdall -e \
      sametime-connect -e skype -e worldofgoo -e android-notifier-desktop -e \
       webcamstudio -e MATE -e mate -e caja -e marco -e python-corba -e \
       jdownloader'
