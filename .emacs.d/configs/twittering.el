;; -*- mode: emacs-lisp -*-

(setq twittering-use-master-password t)
(setq twittering-icon-mode t)                ; Show icons

(global-set-key "\C-crt" 'twittering-native-retweet)
(global-set-key "\C-csu" 'twittering-tinyurl-replace-at-point)
(global-set-key "\C-csr" 'twittering-show-replied-statuses)

(add-hook 'twittering-mode-hook
  (lambda ()
    (mapc (lambda (pair)
      (let ((key (car pair))
        (func (cdr pair)))
          (define-key twittering-mode-map
            (read-kbd-macro key) func)))
        '(("F" . twittering-friends-timeline)
          ("R" . twittering-replies-timeline)
          ("U" . twittering-user-timeline)
          ("W" . twittering-update-status-interactive)
          (">" . twittering-reply-to-user)
          ("H" . twittering-home-timeline)))))

;; (add-hook 'twittering-new-tweets-hook (lambda ()
;;    (let ((n twittering-new-tweets-count))
;;      (start-process "twittering-notify" nil "notify-send"
;;                     "-i" "/home/joseluis/twemacs.png"
;;                     "¡Twit!"
;;                     (format "Tienes %d new tweet%s"
;;                             n (if (> n 1) "s" ""))))))
