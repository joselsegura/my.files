;; Change text font size
(global-set-key [C-mouse-4] '(lambda () (interactive)
                               (global-text-scale-adjust 1)))
(global-set-key [(control ?+)] '(lambda () (interactive)
                                  (global-text-scale-adjust 1)))
(global-set-key [C-mouse-5] '(lambda () (interactive)
                              (global-text-scale-adjust -1)))
(global-set-key [(control ?-)] '(lambda () (interactive)
                                 (global-text-scale-adjust -1)))
(global-set-key (kbd "C-0") '(lambda () (interactive)
                              (global-text-scale-adjust
                               (- text-scale-mode-amount))
                              (global-text-scale-mode -1)))
