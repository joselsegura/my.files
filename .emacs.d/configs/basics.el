;; -*- coding: utf-8 -*-

;; BASIC CUSTOMIZATION
;; --------------------------------------

(setq inhibit-startup-message t) ;; hide the startup message
(load-theme 'material t) ;; load material theme
(global-linum-mode t) ;; enable line numbers globally
(line-number-mode t)
(column-number-mode t)
(elpy-enable)

(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Deleting the trailinbg whitespaces and new-lines
(add-hook 'write-file-hooks 'delete-trailing-whitespace)
(setq delete-trailing-whitespace-p t)

;; set title name
(setq frame-title-format "Emacs -- %b")
(toggle-scroll-bar -1)

;; enable which-function-mode
(which-function-mode t)
