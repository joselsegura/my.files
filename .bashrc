[ -z "$PS1" ] && return

export HISTCONTROL="erasedups:ignoreboth"

shopt -s histappend
shopt -s checkwinsize

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if [ -z `which emacs24` ]; then
    export EDITOR=emacs
else
    export EDITOR=emacs24
fi

export PS1='\[\e[0m\]\u@\[\e[1;31m\]\h\[\e[0m\]:\[\e[1;32m\]\w \[\e[1;34m\]\t
$([[ $? = 0 ]] && echo "\[\e[1;32m\]" || echo "\[\e[1;31m\]")\$\[\e[0m\] '

source ~/src/bash-git-prompt/gitprompt.sh
GIT_PROMPT_ONLY_IN_REPO=1

# turn on numlock on terminals
if [[ tty == /dev/tty* ]];  then
    setleds +num
fi

# archivo para tener cosas temporales para el bash
if [ -f ~/.bash_temp ]; then
    . ~/.bash_temp
fi

eval $(lesspipe)
export LESS=' -R '
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"

# Debian stuff
export DEBFULLNAME="José Luis Segura Lucas"
export DEBEMAIL="josel.segura@gmx.es"

export QUILT_PATCHES=debian/patches

# functions section
alternate() {
  if [[ ! $# -eq 2 ]]; then
    echo "Error: two arguments required" 1>&2
    false
    return
  fi

  if [[ ! -e $1 || ! -e $2 ]]; then
    echo "Error: one (or both) files doesn't exist" 1>&2
    false
    return
  fi

  TMPFILE=`tempfile`
  cp $1 $TMPFILE
  cp $2 $1
  cp $TMPFILE $2
  rm -f $TMPFILE
}
